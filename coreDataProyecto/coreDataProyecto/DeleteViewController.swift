//
//  DeleteViewController.swift
//  coreDataProyecto
//
//  Created by andrea villacis on 30/1/18.
//  Copyright © 2018 andrea villacis. All rights reserved.
//

import UIKit

class DeleteViewController: UIViewController {

    
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var phoneLabel: UILabel!
    
    var person:Person?
   
    override func viewDidLoad() {
        super.viewDidLoad()

        title = person?.name!
        addressLabel.text = person?.address
        phoneLabel.text = person?.phone
        
        // Do any additional setup after loading the view.
    }

    
    @IBAction func deleteButtonPressed(_ sender: Any) {
    let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        manageObjectContext.delete(person!)
        
        do{
            try manageObjectContext.save()
            
        }catch{
            print("error deleting object")
        }
    }
    
}
