//
//  ViewController.swift
//  coreDataProyecto
//
//  Created by andrea villacis on 23/1/18.
//  Copyright © 2018 andrea villacis. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    
    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var addressTextField: UITextField!
    
    @IBOutlet weak var phoneTextField: UITextField!
    
    var personaArray: [Person]  = []
    
    
    let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var singlePerson:Person?
    //var personArray:[Person] = []
    
    func savePerson(){
        
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        let person = Person(entity: entityDescription!, insertInto: manageObjectContext)
        person.name = nameTextField.text ?? ""
        person.address = addressTextField.text ?? ""
        person.phone = phoneTextField.text ?? ""
        
        do {
            
            try manageObjectContext.save()
            clearFields()
            
            
        }catch{
            print("error")
        }
    }
    
    func clearFields(){
        addressTextField.text = ""
        nameTextField.text = ""
        phoneTextField.text = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        savePerson()
    }
    
    
    @IBAction func findButtonPressed(_ sender: Any) {
        if nameTextField.text == ""{
           findAll()
            return
        }
        findOne()
    }
    
    func findAll(){
        //let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        
        do{
            let results = try(manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            
            for result in results{
                let personita = result as! Person
                personaArray.append(personita)
                //print(result)
                let person = result as! Person
                print("name: \(person.name ?? "") ", terminator:"")
                print("Address: \(person.address ?? "") ", terminator:"")
                print("phone: \(person.phone ?? "") ", terminator:"")
                print()
                print()
            }
            performSegue(withIdentifier: "MuestraSegue", sender: self)
            personaArray.removeAll()
            
        }catch{
            print("error finding people")
        }
    }
    
    func findOne(){
        
         let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        request.entity = entityDescription
        let predicate = NSPredicate(format: "name = %@", nameTextField.text!)
        request.predicate = predicate
        
        
        
        do{
            let results = try(manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            
            if results.count  > 0 {
                let match = results[0] as! Person
//                addressTextField.text = match.address
//                nameTextField.text = match.name
//                phoneTextField.text = match.phone
                singlePerson=match
                performSegue(withIdentifier: "DeleteSegue", sender: self)
                
            } else{
                addressTextField.text = "n/a"
                nameTextField.text = "n/a"
                phoneTextField.text = "n/a"
                
            }
            
        }catch{
            print("error finding one")
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MuestraSegue"{
            let destination = segue.destination as! TableViewController
            destination.personaArray = personaArray
            //destination.person = singlePerson
        }
        
    }
}

