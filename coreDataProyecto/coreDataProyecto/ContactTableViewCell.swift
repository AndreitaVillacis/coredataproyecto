//
//  ContactTableViewCell.swift
//  coreDataProyecto
//
//  Created by andrea villacis on 30/1/18.
//  Copyright © 2018 andrea villacis. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {

    
   
    @IBOutlet weak var addresLabel: UILabel!
    
    @IBOutlet weak var phoneLabel: UILabel!
    
    
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func fillData(persona:Person){
        
        nameLabel.text = persona.name
        addresLabel.text = persona.address
        phoneLabel.text = persona.phone
    }
    
}
