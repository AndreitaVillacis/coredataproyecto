//
//  TableViewController.swift
//  coreDataProyecto
//
//  Created by andrea villacis on 30/1/18.
//  Copyright © 2018 andrea villacis. All rights reserved.
//

import UIKit
import CoreData

class TableViewController:UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    
   
    @IBOutlet weak var PersonaTableView: UITableView!

    
    var personaArray: [Person]  = []
    var index = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return personaArray.count
    }
    
    func getVContacts(personas: [Person]) {
        personaArray = personas
        PersonaTableView.reloadData()//para q muestre en la tabla
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        index = indexPath.row
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell") as! ContactTableViewCell
        
        cell.fillData(persona: personaArray[indexPath.row])
        
        return cell
    }
    
    
   
    
    
}
